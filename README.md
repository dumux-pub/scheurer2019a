Summary
=======

This is the DuMuX module containing the code for producing the results
published in:

Model Similarities for Microbially Induced Calcite Precipitation via Surrogate-Based Bayesian Model Justifiability Analysis

S. Scheurer, A. Schäfer Rodrigues Silva, F. Mohammadi F., J. Hommel, S. Oladyshkin, W. Nowak


Installation
============
The content of this DUNE module was extracted from the module dumux-mineralization.
In particular, the following subfolders of dumux have been extracted:

appl/icp/micp

You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.

dune-common               releases/2.6
dune-istl                 releases/2.6
dune-geometry             releases/2.6
dune-grid                 releases/2.6
dune-localfunctions       releases/2.6
dumux                     releases/3.0


Applications
============

The results are obtained by compiling and running the programs from the sources listed below, which can be found in

appl/icp/micp:

micp.cc		-> make micp (for "regular" model as published by Hommel et al. 2015, see dumux-pub module Hommel2014a) -> ./micp column8.input
        -> make micp_simple_chemistry (for "simplified chemistry" model)
			-> ./micp_simple_chemistry column8.input
        -> make micp_no_susp_bio (for "no suspended biomass" model)
			-> ./micp_no_susp_bio column8_nosuspendedbiomass.input



TODO: Creating surrogate models
===============================

The created executable files and the corresponding input files need to be copied to the corresponding folder from “surrogate-based_bayesianom_model_justifiability_analysis”:
- micp , column8.input -> full_complexity_model
- micp_simple_chemistry, column8.input -> simple_chemistry_model
- micp_no_susp_bio, column8_nosuspendedbiomass.input -> no_suspended_biomass model

For full_complexity_model and no_suspended_biomass_model:
- start {}_MainRun_1.m files (preparation for parallel evaluations) in corresponding folders on a computational cluster (for parallel evaluations) e.g. with shell scripts run_MICP_FC_1.sh and run_MICP_IB_1.sh
- when finished start run_MICP_FC_2.sh and run_MICP_IB_2.sh (which start {}_MainRun_2.m files) for Bayesian updating of the models

For simple_chemistry_model:
- start SCM_MainRun_1.m and SCM_MainRun_2.m files successively (only splitted for saving the not updated models) in the corresponding folder, e.g. with shell scripts run_MICP_SC_1.sh and run_MICP_SC_2.sh

The corresponding outputs will be saved in model subfolders “SimulationX” for each run X). All needed files fo further analysis are extracted from the subfolders automatically and can be found in simple_chemistry_model, full_complexity_model and no_suspended_biomass_model directly.



TODO: Justifiability Analysis
===============================
All folders can be found in “Surrogate-Based Bayesian Model Justifiability Analysis”.

For evaluation of the LOOCV error:
- loocevolution.m (Figure 4) in loocv_evolution folder
- move needed files, that are loaded at the beginning of loocvevolution.m to this folder and rename them as named in the load options (former files are in the model folders)
- the results and needed model files from the last analysis are saved and still in the folder but can be subscribed
- run loocvevolution.m


For the justifiability analysis:

- matrices{}.m (Figure 6), weightbar.m (Figure 5), weighteval.m (Figure 7), fitquality.m (Figure 8) in justifiability_analysis folder
- move needed files, that are loaded at the beginning of files to this folder and rename them as named in the load options (former files are in the model folders)
- the results and needed model files from the last analysis are saved and still in the folder but can be subscribed
- run weightier.m, weighteval.m, fit quality.m
- the {}matrices.m files can be run on a computational cluster in order to save computational run time, e.g. with shell scripts run_MAT_All.sh, run_MAT_Half.sh, run_MAT_Last.sh













