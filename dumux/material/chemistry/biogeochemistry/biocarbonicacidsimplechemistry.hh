/*
 * bioCarbonicAcid.hh
 *
 *  Created on: 9.8.2011
 *      Author: hommel
 */

#ifndef BIO_CARBONIC_ACID_HH_
#define BIO_CARBONIC_ACID_HH_

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>
//#include <dumux/material/binarycoefficients/brine_co2.hh>
#include <dumux/material/fluidsystems/biofluidsystem.hh>
#include <dumux/implicit/2pbiomin/properties.hh>
#include <dumux/material/components/h2o.hh>

#include <cmath>
#include <iostream>
#include <dumux/common/math.hh>

namespace Dumux
{
/*!
 * \brief The equilibrium chemistry is calculated in this class. The function calculateEquilbriumChemistry is used to
 * control the Newton Solver "newton1D". The chemical functions and derivations are implemented in the private part of
 * class.
 */
template <class TypeTag, class CO2Tables>
class BioCarbonicAcid
{
    typedef GetPropType<TypeTag, Properties::Scalar> Scalar;
    typedef GetPropType<TypeTag, Properties::FluidSystem> FluidSystem;
    typedef GetPropType<TypeTag, Properties::VolumeVariables> VolumeVariables;
    typedef GetPropType<TypeTag, Properties::PrimaryVariables> PrimaryVariables;
    typedef GetPropType<TypeTag, Properties::GridView> GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef GetPropType<TypeTag, Properties::FVElementGeometry> FVElementGeometry;

    typedef BioCarbonicAcid<TypeTag, CO2Tables> ThisType;
    typedef Dumux::H2O<Scalar> H2O;


public:

    BioCarbonicAcid()
{
    try
    {

        // biomass parameters
        ca1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, ca1);
        ca2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, ca2);
        cd1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, cd1);
        dc0_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, dc0);
        kmue_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, kmue);
        F_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, F);
        Ke_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, Ke);
        Ks_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, Ks);
        Yield_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, Yield);

        //ureolysis kinetic parameters
        kub_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, UreolysisCoefficients, kub);
        kurease_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, UreolysisCoefficients, kurease);
        Ku_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, UreolysisCoefficients, Ku);

    }
    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }
}

    static const int wPhaseIdx = FluidSystem::wPhaseIdx;
    static const int nPhaseIdx = FluidSystem::nPhaseIdx;

    static const int wCompIdx = FluidSystem::wCompIdx;
    static const int nCompIdx = FluidSystem::nCompIdx;

    static const int H2OIdx = FluidSystem::H2OIdx;
    static const int CTotIdx = FluidSystem::TCIdx;
    static const int CaIdx = FluidSystem::CaIdx;
    static const int NaIdx = FluidSystem::NaIdx;
    static const int ClIdx = FluidSystem::ClIdx;
    static const int BiosuspIdx = FluidSystem::BiosuspIdx;
    static const int BiosubIdx = FluidSystem::BiosubIdx;
    static const int UreaIdx = FluidSystem::UreaIdx;
    static const int O2Idx = FluidSystem::O2Idx;

    static const int CalciteIdx = SolidSystem::CalciteIdx;
    static const int BiofilmIdx = SolidSystem::BiofilmIdx;

    static const int numComponents = FluidSystem::numComponents;

    static const int phiBiofilmIdx = numComponents + BiofilmIdx;
    static const int phiCalciteIdx = numComponents + CalciteIdx;

    typedef Dune::FieldVector<Scalar, 2> SolVector;

    typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;


    /*!
     * \brief Returns the molality of a component for a given mole fraction
     * The salinity and the mole fraction of CO2 are considered
     *
     */
    static Scalar moleFracToMolality(Scalar moleFracX, Scalar moleFracSalinity, Scalar moleFracCTot)
    {
        if(moleFracX<0)
            moleFracX=0;
        if(moleFracSalinity<0)
            moleFracSalinity=0;
        if(moleFracCTot<0)
            moleFracCTot=0;

        Scalar molalityX = moleFracX / (1 - moleFracSalinity - moleFracCTot) / FluidSystem::molarMass(H2OIdx);
        return molalityX;
    }


   void reactionSource(PrimaryVariables &q,
           const VolumeVariables &volVars,
           const Scalar absgradpw,
           const Scalar dt)
           {
//      //define and compute some parameters for siplicity:
     Scalar porosity = volVars.porosity();
     Scalar initialPorosity = volVars.initialPorosity();
     Scalar Sw  =  volVars.saturation(wPhaseIdx);
     Scalar xlSalinity = volVars.moleFracSalinity();
     Scalar densityBiofilm = solidComponentDensity(BiofilmIdx);
     Scalar densityCalcite = solidComponentDensity(CalciteIdx);
     Scalar cBio = volVars.moleFraction(wPhaseIdx, BiosuspIdx) * volVars.molarDensity(wPhaseIdx) * FluidSystem::molarMass(BiosuspIdx);      //[kg_suspended_Biomass/m³_waterphase]
     if(cBio < 0)
         cBio = 0;
     Scalar volFracCalcite = volVars.solidVolumeFraction(CalciteIdx);
     if (volFracCalcite < 0)
         volFracCalcite = 0;
     Scalar volFracBiofilm = volVars.solidVolumeFraction(BiofilmIdx);
     if (volFracBiofilm < 0)
         volFracBiofilm = 0;
     Scalar massBiofilm = densityBiofilm * volFracBiofilm;
     Scalar cSubstrate = volVars.moleFraction(wPhaseIdx, BiosubIdx) * volVars.molarDensity(wPhaseIdx) * FluidSystem::molarMass(BiosubIdx);  //[kg_substrate/m³_waterphase]
     if(cSubstrate < 0)
         cSubstrate = 0;
     Scalar cO2 = volVars.moleFraction(wPhaseIdx, O2Idx) * volVars.molarDensity(wPhaseIdx) * FluidSystem::molarMass(O2Idx);                 //[kg_oxygen/m³_waterphase]
     if(cO2 < 0)//1e-10)
         cO2 = 0;

     Scalar mUrea = moleFracToMolality(volVars.moleFraction(wPhaseIdx,UreaIdx), xlSalinity, volVars.moleFraction(wPhaseIdx,nCompIdx));  //[mol_urea/kg_H2O]
     if (mUrea < 0)
         mUrea = 0;

     // compute rate of urealysis:
     Scalar vmax = kurease_; //According to new experimental results without pH dependence
     Scalar Zub = kub_ *  massBiofilm;  // [kgurease/m³]
     Scalar rurea = vmax * Zub * mUrea / (Ku_ + mUrea); //[mol/m³s]


     // compute dissolution and precipitation rate of calcite
     Scalar rdiss = 0;
     Scalar rprec = 0;

     if(volVars.moleFraction(wPhaseIdx,CaIdx)>1e-8)
         rprec = rurea;

     //compute biomass growth coefficient and rate
     Scalar mue = kmue_ * cSubstrate / (Ks_ + cSubstrate) * cO2 / (Ke_ + cO2);// [1/s]
     Scalar rgf = mue * massBiofilm;                //[kg/m³s]
     Scalar rgb = mue * porosity * Sw * cBio;   //[kg/m³s]

     if(cO2-(rgf+rgb)*dt<0)
     {
         mue =  mue * 0.99*cO2/((rgf+rgb)*dt);
         rgf = mue * massBiofilm;
         rgb = mue * porosity * Sw * cBio;
     }

     // compute biomass decay coefficient and rate:
     Scalar dcf = dc0_;
      dcf += rprec * SolidSystem::molarMass(CalciteIdx) /               //[1/s]
             (densityCalcite * (initialPorosity - volFracCalcite));
//   Scalar dcb = dc0_ * (1 + (mH * mH)/KpHa_ + KpHb_/mH); //KpHb_ = 0!!!!!!        //[1/s]
     Scalar dcb = dc0_;     //[1/s]
     Scalar rdcf = dcf * massBiofilm; //[kg/m³s]
     Scalar rdcb = dcb * porosity * Sw * cBio;      //[kg/m³s]

     // compute attachment coefficient and rate:
     Scalar ka = ca1_ * volFracBiofilm + ca2_;          //[1/s]
     Scalar ra = ka * porosity * Sw * cBio;             //[kg/m³s]

     // compute detachment coefficient and rate:
     Scalar cd2 = volFracBiofilm / (initialPorosity - volFracCalcite);      //[-]
     Scalar kd = cd1_ * pow((porosity * Sw * absgradpw),0.58) + cd2 * mue;  //[1/s]
     Scalar rd = kd * massBiofilm;                      //[kg/m³s]

     // rdiss+rprec[mol/m³s]
     // rurea[mol/m³s]
     // rgb + rdcb + ra + rd [kg/m³s]
     // q[kg/m³s]
     q[wCompIdx] += 0;
     q[nCompIdx] += rurea - rprec + rdiss;
     q[NaIdx] += 0;
     q[ClIdx] += 0;
     q[CaIdx] += - rprec + rdiss;
     q[UreaIdx] += - rurea;
     q[TNHIdx] += 2 * rurea;
     q[O2Idx] += -(rgf + rgb) *F_/Yield_ / FluidSystem::molarMass(O2Idx);
     q[BiosubIdx] += -(rgf + rgb) / Yield_ / FluidSystem::molarMass(BiosubIdx);
     q[BiosuspIdx] += (rgb - rdcb - ra + rd) / FluidSystem::molarMass(BiosuspIdx);
     q[phiBiofilmIdx] += (rgf - rdcf + ra - rd) / SolidSystem::molarMass(BiofilmIdx);
     q[phiCalciteIdx] += + rprec - rdiss;

}

private:
    // biomass parameters
    Scalar ca1_;
    Scalar ca2_;
    Scalar cd1_;
    Scalar dc0_;
    Scalar kmue_ ;
    Scalar F_;
    Scalar Ke_;
    Scalar Ks_;
    Scalar Yield_;

    // urease parameters
    Scalar kub_;
    Scalar kurease_;
    Scalar Ku_;

};

} // end namespace

#endif