#!/bin/sh

### Create a folder for the DUNE and DuMuX modules
### Go into the folder and execute this script
mkdir Scheurer2019a
cd Scheurer2019a

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/staging/dune-uggrid.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/scheurer2019a.git scheurer2019a

### Go to specific branches
cd dune-common && git checkout releases/2.6 && cd ..
cd dune-geometry && git checkout releases/2.6 && cd ..
cd dune-grid && git checkout releases/2.6 && cd ..
cd dune-istl && git checkout releases/2.6 && cd ..
cd dune-localfunctions && git checkout releases/2.6 && cd ..
cd dune-uggrid && git checkout releases/2.6 && cd ..
cd dumux && git checkout releases/3.1 && cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
