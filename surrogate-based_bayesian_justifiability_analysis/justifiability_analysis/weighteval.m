% ========================================================================
% ====== Average model weight for the data-generating model over increasing
% ====== amount of spacial data points
% ========================================================================

clear all;


%load data
load('AllPoints_all.mat','Norm_corr_Post_matrix_Ct');
load('AllPoints_all.mat','Norm_corr_Post_matrix_Ca');
AllPoints_Norm_corr_Post_matrix_Ct = Norm_corr_Post_matrix_Ct;
AllPoints_Norm_corr_Post_matrix_Ca = Norm_corr_Post_matrix_Ca;
load('LastHalfOfPoints_all.mat','Norm_corr_Post_matrix_Ct');
load('LastHalfOfPoints_all.mat','Norm_corr_Post_matrix_Ca');
LastHalfOfPoints_Norm_corr_Post_matrix_Ct = Norm_corr_Post_matrix_Ct;
LastHalfOfPoints_Norm_corr_Post_matrix_Ca = Norm_corr_Post_matrix_Ca;
load('LastPoint_all.mat','Norm_corr_Post_matrix_Ct');
load('LastPoint_all.mat','Norm_corr_Post_matrix_Ca');
LastPoint_Norm_corr_Post_matrix_Ct = Norm_corr_Post_matrix_Ct;
LastPoint_Norm_corr_Post_matrix_Ca = Norm_corr_Post_matrix_Ca;


%initialize
N = 4; %models
M = 3; %data set sizes
xax_Ct = [0,1,4,8];
xax_Ca = [0,1,3,5];

%diagonal elements
for i=1:N
    AllPoints_Ct(i) = AllPoints_Norm_corr_Post_matrix_Ct(i,i);
    AllPoints_Ca(i) = AllPoints_Norm_corr_Post_matrix_Ca(i,i);
    LastHalfOfPoints_Ct(i) = LastHalfOfPoints_Norm_corr_Post_matrix_Ct(i,i);
    LastHalfOfPoints_Ca(i) = LastHalfOfPoints_Norm_corr_Post_matrix_Ca(i,i);
    LastPoint_Ct(i) = LastPoint_Norm_corr_Post_matrix_Ct(i,i);
    LastPoint_Ca(i) = LastPoint_Norm_corr_Post_matrix_Ca(i,i);
end
Ct = [LastPoint_Ct,LastHalfOfPoints_Ct,AllPoints_Ct];
Ca = [LastPoint_Ca,LastHalfOfPoints_Ca,AllPoints_Ca];

MD_Ct = 0.25;
SC_Ct = 0.25;
IB_Ct = 0.25;
FC_Ct = 0.25;
MD_Ca = 0.25;
SC_Ca = 0.25;
IB_Ca = 0.25;
FC_Ca = 0.25;
for i=1:M
    MD_Ct(i+1) = Ct(i*N-3);
    SC_Ct(i+1) = Ct(i*N-2);
    IB_Ct(i+1) = Ct(i*N-1);
    FC_Ct(i+1) = Ct(i*N);
    MD_Ca(i+1) = Ca(i*N-3);
    SC_Ca(i+1) = Ca(i*N-2);
    IB_Ca(i+1) = Ca(i*N-1);
    FC_Ca(i+1) = Ca(i*N);
end

%calcite
figure(1)
subplot(1,2,1);
plot(xax_Ct,MD_Ct,'-gs','LineWidth',1,'Color',[86/255,211/255,47/255],'MarkerSize',15,'LineWidth',2);
hold on;
plot(xax_Ct,SC_Ct,'-gs','LineWidth',1,'Color',[48/255,118/255,182/255],'MarkerSize',15,'LineWidth',2);
plot(xax_Ct,IB_Ct,'-gs','LineWidth',1,'Color',[237/255,109/255,87/255],'MarkerSize',15,'LineWidth',2);
plot(xax_Ct,FC_Ct,'-gs','LineWidth',1,'Color',[241/255,151/255,55/255],'MarkerSize',15,'LineWidth',2);
hold off;
title('Calcite content');
legend('MD','SC','IB','FC');
xlim([0 8.1]);
%ylim = [0, 1.1];
xlabel('number of data points N_{D,spacial}');
ylabel('model weight');
set(gca,'FontSize',20);
set(gca, 'FontName', 'Times New Roman');
grid();


%calcium
subplot(1,2,2);
plot(xax_Ca,MD_Ca,'-gs','LineWidth',1,'Color',[86/255,211/255,47/255],'MarkerSize',15,'LineWidth',2);
hold on;
plot(xax_Ca,SC_Ca,'-gs','LineWidth',1,'Color',[48/255,118/255,182/255],'MarkerSize',15,'LineWidth',2);
plot(xax_Ca,IB_Ca,'-gs','LineWidth',1,'Color',[237/255,109/255,87/255],'MarkerSize',15,'LineWidth',2);
plot(xax_Ca,FC_Ca,'-gs','LineWidth',1,'Color',[241/255,151/255,55/255],'MarkerSize',15,'LineWidth',2);
hold off;
title('Calcium concentration');
legend('MD','SC','IB','FC');
xlim([0 5.1]);
%ylim = [0, 1.1];
xlabel('number of data points N_{D,spacial}');
ylabel('model weight');
set(gca,'FontSize',20);
set(gca, 'FontName', 'Times New Roman');
grid();

sgt = sgtitle('Evolution of model weights for data-generating process');
sgt.FontSize = 24;
set(sgt,'FontWeight','bold');
set(sgt, 'FontName', 'Times New Roman');

