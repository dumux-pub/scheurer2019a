% ========================================================
% ====== posterior matrices for half of the dataset
% ========================================================



%initialize
clear all;
N = 4;
N_IB = 2;
N_RS = 1000;
SC_P = 15;
FC_P = 15;
IB_P = 6;
numberOfPointsTime = 35;
numberOfPointsSpaceKnown = 3;
numberOfPointsSpace = 57;


%load data
%have to be copied and renamed from the corresponding model folders
%-----------------------------------------------------------------------

%load Coeffs, Psi, PolynomialDegree for surrogate model evaluation and
%collocation point evaluation
load SC_CoeffsCt.mat;
SC_CoeffsCt = CoeffsCt;
load SC_CoeffsCa.mat;
SC_CoeffsCa = CoeffsCa;
load FC_CoeffsCt.mat;
FC_CoeffsCt = CoeffsCt;
load FC_CoeffsCa.mat;
FC_CoeffsCa = CoeffsCa;
load IB_CoeffsCt.mat;
IB_CoeffsCt = CoeffsCt;
load IB_CoeffsCa.mat;
IB_CoeffsCa = CoeffsCa;
load('SC_ManagementData.mat','PolynomialDegree');
SC_PolynomialDegree = PolynomialDegree;
load('FC_ManagementData.mat','PolynomialDegree');
FC_PolynomialDegree = PolynomialDegree;
load('IB_ManagementData.mat','PolynomialDegree');
IB_PolynomialDegree = PolynomialDegree;


%load output data and Psi for correction factors
load SC_Psi.mat;
SC_Psi = Psi;
load FC_Psi.mat;
FC_Psi= Psi;
load IB_Psi.mat;
IB_Psi= Psi;
load SC_OutputCt.mat;
SC_OutputCt = OutputCt;
load SC_OutputCa.mat;
SC_OutputCa = OutputCa;
load FC_OutputCt.mat;
FC_OutputCt = OutputCt;
load FC_OutputCa.mat;
FC_OutputCa= OutputCa;
load IB_OutputCt.mat;
IB_OutputCt = OutputCt;
load IB_OutputCa.mat;
IB_OutputCa= OutputCa;


%load measurement data, create error variance matrix, reshape calcium
%measurement data
load observedCt.mat;
load measuredCa.mat;
load StdCt.mat;

measuredCa = measuredCa(:,3:5);
observedCa = reshape(measuredCa',1,numel(measuredCa));
observedCt = observedCt(5:8)';
observedCa = observedCa';

RCt = diag((0.2.*observedCt).^2);
RCa = diag((0.2.*observedCa).^2);


%set polynomial bases for FC and SC model
for i=1:N
    SC_Name = strcat('SC_PolynomialBasis_',num2str(i),'.mat');
    SC_PolynomialBasisFileName{i} = SC_Name;
    FC_Name = strcat('FC_PolynomialBasis_',num2str(i),'.mat');
    FC_PolynomialBasisFileName{i} = FC_Name;
end


%set polynomial bases for IB model
for i=1:N/2
    IB_Name = strcat('IB_PolynomialBasis_',num2str(i),'.mat');
    IB_PolynomialBasisFileName{i} = IB_Name;
end


%prior sampling/ MC sampling/ random sampling
%set MC evaluation points
MC_Points = zeros(N_RS,N);
MC_Points(:,1) = random('unif',10^-10,10^-7,N_RS,1); %ca1,1
MC_Points(:,2) = random('unif',10^-10,10^-6,N_RS,1); %ca2,2
MC_Points(:,3) = random('unif',1,15,N_RS,1); %rhoBiofilm,3
MC_Points(:,4) = random('unif',10^-5,5*10^-4,N_RS,1); %kub,4
IB_MC_Points =  MC_Points(:,3:4);


%evaluate bases on MC Points for SC
for i=1:1:SC_P
    for j=1:1:N_RS
        SC_Basis(i,j)=1;
        for ii=1:1:N
            load(SC_PolynomialBasisFileName{ii},'Polynomial');
            SC_Basis(i,j)=SC_Basis(i,j)*polyval(Polynomial(1+SC_PolynomialDegree(i,ii),length(Polynomial):-1:1),MC_Points(j,ii)); % evaluation in each integration point
        end
    end
end

%evaluate bases on MC Points for FC
for i=1:1:FC_P
    for j=1:1:N_RS
        FC_Basis(i,j)=1;
        for ii=1:1:N
            load(FC_PolynomialBasisFileName{ii},'Polynomial');
            FC_Basis(i,j)=FC_Basis(i,j)*polyval(Polynomial(1+FC_PolynomialDegree(i,ii),length(Polynomial):-1:1),MC_Points(j,ii)); % evaluation in each integration point
        end
    end
end

%evaluate bases on MC Points for IB
for i=1:1:IB_P
    for j=1:1:N_RS
        IB_Basis(i,j)=1;
        for ii=1:1:N_IB
            load(IB_PolynomialBasisFileName{ii},'Polynomial');
            IB_Basis(i,j)=IB_Basis(i,j)*polyval(Polynomial(1+IB_PolynomialDegree(i,ii),length(Polynomial):-1:1),IB_MC_Points(j,ii)); % evaluation in each integration point
        end
    end
end


%compute SC surrogate model output via coefficients and basis
%calcite
SC_SurrogateCt = SC_Basis'*SC_CoeffsCt;
%Calcium
for i = 1:numberOfPointsTime
    SC_SurrogateCaa(i,:,:) = (SC_Basis'*reshape(SC_CoeffsCa(i,:,:),numberOfPointsSpace,SC_P)')';
end

%compute FC surrogate model output via coefficients and basis
%calcite
FC_SurrogateCt = FC_Basis'*FC_CoeffsCt;
%calcium
for i = 1:numberOfPointsTime
    FC_SurrogateCaa(i,:,:) = (FC_Basis'*reshape(FC_CoeffsCa(i,:,:),numberOfPointsSpace,FC_P)')';
end

%compute IB surrogate model output via coefficients and basis
%Calcite
IB_SurrogateCt = IB_Basis'*IB_CoeffsCt;
%Calcium
for i = 1:numberOfPointsTime
    IB_SurrogateCaa(i,:,:) = (IB_Basis'*reshape(IB_CoeffsCa(i,:,:),numberOfPointsSpace,IB_P)')';
end


%only measured points in space, SC
SC_SurrogateCt = [SC_SurrogateCt(:,28),SC_SurrogateCt(:,34),...
    SC_SurrogateCt(:,40),SC_SurrogateCt(:,46)];
SC_SurrogateCaa = [SC_SurrogateCaa(:,24,:),...
    SC_SurrogateCaa(:,31,:),SC_SurrogateCaa(:,39,:)];
%only measured points in space, FC
FC_SurrogateCt = [FC_SurrogateCt(:,28),FC_SurrogateCt(:,34),...
    FC_SurrogateCt(:,40),FC_SurrogateCt(:,46)];
FC_SurrogateCaa = [FC_SurrogateCaa(:,24,:),...
    FC_SurrogateCaa(:,31,:),FC_SurrogateCaa(:,39,:)];
%only measured points in space, IB
IB_SurrogateCt = [IB_SurrogateCt(:,28),IB_SurrogateCt(:,34),...
    IB_SurrogateCt(:,40),IB_SurrogateCt(:,46)];
IB_SurrogateCaa = [IB_SurrogateCaa(:,24,:),...
    IB_SurrogateCaa(:,31,:),IB_SurrogateCaa(:,39,:)];


%reshape Calcium, all points in time lined up
for i = 1:N_RS
    SC_SurrogateCa(i,:) = reshape(squeeze(SC_SurrogateCaa(:,:,i))',1,numberOfPointsSpaceKnown*numberOfPointsTime);
end
for i = 1:N_RS
    FC_SurrogateCa(i,:) = reshape(squeeze(FC_SurrogateCaa(:,:,i))',1,numberOfPointsSpaceKnown*numberOfPointsTime);
end
for i = 1:N_RS
    IB_SurrogateCa(i,:) = reshape(squeeze(IB_SurrogateCaa(:,:,i))',1,numberOfPointsSpaceKnown*numberOfPointsTime);
end


%stick all surrogate/measurement data
outputCt = [observedCt,SC_SurrogateCt',IB_SurrogateCt',FC_SurrogateCt'];
outputCa = [observedCa,SC_SurrogateCa',IB_SurrogateCa',FC_SurrogateCa'];


%set up likelihood matrix with all likelihood entries
amount = 1+3*N_RS;
likelihood_Matrix_Ct = zeros(amount);
likelihood_Matrix_Ca = zeros(amount);

for i = 1:amount
    for j = i:amount
        if i == 1
            likelihood_Matrix_Ct(i,j) = exp(-0.5*(outputCt(:,i)-outputCt(:,j))'*pinv(RCt)*(outputCt(:,i)-outputCt(:,j)));
            likelihood_Matrix_Ca(i,j) = exp(-0.5*10^(-2)*(outputCa(:,i)-outputCa(:,j))'*pinv(RCa)*(outputCa(:,i)-outputCa(:,j)));
        else
            SCt = diag((0.2.*outputCt(:,i)).^2);
            SCa = diag((0.2.*outputCa(:,i)).^2);
            S = diag([(0.2.*outputCt(:,i)).^2;(0.2.*outputCa(:,i)).^2]);
            likelihood_Matrix_Ct(i,j) = exp(-0.5*(outputCt(:,i)-outputCt(:,j))'*pinv(SCt)*(outputCt(:,i)-outputCt(:,j)));
            likelihood_Matrix_Ca(i,j) = exp(-0.5*(outputCa(:,i)-outputCa(:,j))'*pinv(SCa)*(outputCa(:,i)-outputCa(:,j)));
        end
        likelihood_Matrix_Ct(j,i) = likelihood_Matrix_Ct(i,j);
        likelihood_Matrix_Ca(j,i) = likelihood_Matrix_Ca(i,j);
    end
end

%posterior denominators
normal_Ct = sum(likelihood_Matrix_Ct(2:end,:));
normal_Ca = sum(likelihood_Matrix_Ca(2:end,:));

%posterior matrix
Post_matrix_Ct = declare_matrix(likelihood_Matrix_Ct,N_RS,normal_Ct);
Post_matrix_Ca = declare_matrix(likelihood_Matrix_Ca,N_RS,normal_Ca);

%normalized posterior matrix
Norm_Post_matrix_Ct = zeros(4);
Norm_Post_matrix_Ca = zeros(4);

for i = 1:4
    for j = 1:4
        Norm_Post_matrix_Ct(i,j) = Post_matrix_Ct(i,j)/sum(Post_matrix_Ct(:,j));
        Norm_Post_matrix_Ca(i,j) = Post_matrix_Ca(i,j)/sum(Post_matrix_Ca(:,j));
    end
end


%normalized posterior values for measurementdata-model comparison
Norm_Post_MD_Ct = zeros(1,3);
Norm_Post_MD_Ca = zeros(1,3);

for i = 1:3
    Norm_Post_MD_Ct(i) = Post_matrix_Ct(i+1,1)/sum(Post_matrix_Ct(2:4,1));
    Norm_Post_MD_Ca(i) = Post_matrix_Ca(i+1,1)/sum(Post_matrix_Ca(2:4,1));
end



%correction factors for the models

%evaluate Psi and Coeffs for surrogate model on CPoints
%calcite
SC_SurrogateCt_CPoints = SC_Psi'*SC_CoeffsCt;
%calcium
for i = 1:numberOfPointsTime
    SC_SurrogateCaa_CPoints(i,:,:) = (SC_Psi'*reshape(SC_CoeffsCa(i,:,:),numberOfPointsSpace,SC_P)')';
end

%calcite
FC_SurrogateCt_CPoints = FC_Psi'*FC_CoeffsCt;
%calcium
for i = 1:numberOfPointsTime
    FC_SurrogateCaa_CPoints(i,:,:) = (FC_Psi'*reshape(FC_CoeffsCa(i,:,:),numberOfPointsSpace,FC_P)')';
end

%calcite
IB_SurrogateCt_CPoints = IB_Psi'*IB_CoeffsCt;
%calcium
for i = 1:numberOfPointsTime
    IB_SurrogateCaa_CPoints(i,:,:) = (IB_Psi'*reshape(IB_CoeffsCa(i,:,:),numberOfPointsSpace,IB_P)')';
end

%only measured points in space, SC (surrogate)
SC_SurrogateCt_CPoints = [SC_SurrogateCt_CPoints(:,4),SC_SurrogateCt_CPoints(:,10),SC_SurrogateCt_CPoints(:,16),...
    SC_SurrogateCt_CPoints(:,22),SC_SurrogateCt_CPoints(:,28),SC_SurrogateCt_CPoints(:,34),...
    SC_SurrogateCt_CPoints(:,40),SC_SurrogateCt_CPoints(:,46)];
SC_SurrogateCaa_CPoints = [SC_SurrogateCaa_CPoints(:,9,:),SC_SurrogateCaa_CPoints(:,16,:),SC_SurrogateCaa_CPoints(:,24,:),...
    SC_SurrogateCaa_CPoints(:,31,:),SC_SurrogateCaa_CPoints(:,39,:)];
%only measured points in space, FC (surrogate)
FC_SurrogateCt_CPoints = [FC_SurrogateCt_CPoints(:,4),FC_SurrogateCt_CPoints(:,10),FC_SurrogateCt_CPoints(:,16),...
    FC_SurrogateCt_CPoints(:,22),FC_SurrogateCt_CPoints(:,28),FC_SurrogateCt_CPoints(:,34),...
    FC_SurrogateCt_CPoints(:,40),FC_SurrogateCt_CPoints(:,46)];
FC_SurrogateCaa_CPoints = [FC_SurrogateCaa_CPoints(:,9,:),FC_SurrogateCaa_CPoints(:,16,:),FC_SurrogateCaa_CPoints(:,24,:),...
    FC_SurrogateCaa_CPoints(:,31,:),FC_SurrogateCaa_CPoints(:,39,:)];
%only measured points in space, IB (surrogate)
IB_SurrogateCt_CPoints = [IB_SurrogateCt_CPoints(:,4),IB_SurrogateCt_CPoints(:,10),IB_SurrogateCt_CPoints(:,16),...
    IB_SurrogateCt_CPoints(:,22),IB_SurrogateCt_CPoints(:,28),IB_SurrogateCt_CPoints(:,34),...
    IB_SurrogateCt_CPoints(:,40),IB_SurrogateCt_CPoints(:,46)];
IB_SurrogateCaa_CPoints = [IB_SurrogateCaa_CPoints(:,9,:),IB_SurrogateCaa_CPoints(:,16,:),IB_SurrogateCaa_CPoints(:,24,:),...
    IB_SurrogateCaa_CPoints(:,31,:),IB_SurrogateCaa_CPoints(:,39,:)];

%only measured points in space, SC (model)
SC_ModelCt = [SC_OutputCt(:,4),SC_OutputCt(:,10),SC_OutputCt(:,16),SC_OutputCt(:,22),SC_OutputCt(:,28),SC_OutputCt(:,34),...
    SC_OutputCt(:,40),SC_OutputCt(:,46)];
SC_ModelCaa = [SC_OutputCa(:,9,:),SC_OutputCa(:,16,:),SC_OutputCa(:,24,:),SC_OutputCa(:,31,:),SC_OutputCa(:,39,:)];
%only measured points in space, FC (model)
FC_ModelCt = [FC_OutputCt(:,4),FC_OutputCt(:,10),FC_OutputCt(:,16),FC_OutputCt(:,22),FC_OutputCt(:,28),FC_OutputCt(:,34),...
    FC_OutputCt(:,40),FC_OutputCt(:,46)];
FC_ModelCaa = [FC_OutputCa(:,9,:),FC_OutputCa(:,16,:),FC_OutputCa(:,24,:),FC_OutputCa(:,31,:),FC_OutputCa(:,39,:)];
%only measured points in space, IB (model)
IB_ModelCt = [IB_OutputCt(:,4),IB_OutputCt(:,10),IB_OutputCt(:,16),IB_OutputCt(:,22),IB_OutputCt(:,28),IB_OutputCt(:,34),...
    IB_OutputCt(:,40),IB_OutputCt(:,46)];
IB_ModelCaa = [IB_OutputCa(:,9,:),IB_OutputCa(:,16,:),IB_OutputCa(:,24,:),IB_OutputCa(:,31,:),IB_OutputCa(:,39,:)];

%reshape Calcium, all points in time lined up
for i = 1:25
    SC_SurrogateCa_CPoints(i,:) = reshape(SC_SurrogateCaa_CPoints(:,:,i)',1,5*numberOfPointsTime);
    SC_ModelCa(i,:) = reshape(SC_ModelCaa(:,:,i)',1,5*numberOfPointsTime);
end
for i = 1:25
    FC_SurrogateCa_CPoints(i,:) = reshape(FC_SurrogateCaa_CPoints(:,:,i)',1,5*numberOfPointsTime);
    FC_ModelCa(i,:) = reshape(FC_ModelCaa(:,:,i)',1,5*numberOfPointsTime);
end
for i = 1:16
    IB_SurrogateCa_CPoints(i,:) = reshape(IB_SurrogateCaa_CPoints(:,:,i)',1,5*numberOfPointsTime);
    IB_ModelCa(i,:) = reshape(IB_ModelCaa(:,:,i)',1,5*numberOfPointsTime);
end


%set covariance matrix of approximation errors
SC_SCt = cov(SC_SurrogateCt_CPoints - SC_ModelCt)';
SC_SCa = cov(SC_SurrogateCa_CPoints - SC_ModelCa)';
FC_SCt = cov(FC_SurrogateCt_CPoints - FC_ModelCt)';
FC_SCa = cov(FC_SurrogateCa_CPoints - FC_ModelCa)';
IB_SCt = cov(IB_SurrogateCt_CPoints - IB_ModelCt)';
IB_SCa = cov(IB_SurrogateCa_CPoints - IB_ModelCa)';

%correction factors
corrCt_SC = 0;
corrCa_SC = 0;
corrCt_FC = 0;
corrCa_FC = 0;
corrCt_IB = 0;
corrCa_IB = 0;

for i = 1:25
    corrCt_SC = corrCt_SC + exp(-0.5*(SC_ModelCt(i,:)-SC_SurrogateCt_CPoints(i,:))*pinv(SC_SCt)*(SC_ModelCt(i,:)-SC_SurrogateCt_CPoints(i,:))');
    corrCa_SC = corrCa_SC + exp(-0.5*(SC_ModelCa(i,:)-SC_SurrogateCa_CPoints(i,:))*pinv(SC_SCa)*(SC_ModelCa(i,:)-SC_SurrogateCa_CPoints(i,:))');
    
    corrCt_FC = corrCt_FC + exp(-0.5*(FC_ModelCt(i,:)-FC_SurrogateCt_CPoints(i,:))*pinv(FC_SCt)*(FC_ModelCt(i,:)-FC_SurrogateCt_CPoints(i,:))');
    corrCa_FC = corrCa_FC + exp(-0.5*(FC_ModelCa(i,:)-FC_SurrogateCa_CPoints(i,:))*pinv(FC_SCa)*(FC_ModelCa(i,:)-FC_SurrogateCa_CPoints(i,:))');
end

for i = 1:16
    corrCt_IB = corrCt_IB + exp(-0.5*(IB_ModelCt(i,:)-IB_SurrogateCt_CPoints(i,:))*pinv(IB_SCt)*(IB_ModelCt(i,:)-IB_SurrogateCt_CPoints(i,:))');
    corrCa_IB = corrCa_IB + exp(-0.5*(IB_ModelCa(i,:)-IB_SurrogateCa_CPoints(i,:))*pinv(IB_SCa)*(IB_ModelCa(i,:)-IB_SurrogateCa_CPoints(i,:))');
end


%correction of the matrices
correction_matrix_Ct = [1,corrCt_SC, corrCt_IB, corrCt_FC; corrCt_SC, corrCt_SC^2, corrCt_SC*corrCt_IB, corrCt_SC*corrCt_FC;...
    corrCt_IB, corrCt_IB*corrCt_SC,corrCt_IB^2, corrCt_IB*corrCt_FC; corrCt_FC, corrCt_FC*corrCt_SC, corrCt_FC*corrCt_IB, corrCt_FC^2];
correction_matrix_Ca = [1,corrCa_SC, corrCa_IB, corrCa_FC; corrCa_SC, corrCa_SC^2, corrCa_SC*corrCt_IB, corrCa_SC*corrCa_FC;...
    corrCa_IB, corrCa_IB*corrCa_SC,corrCa_IB^2, corrCa_IB*corrCa_FC; corrCa_FC, corrCa_FC*corrCa_SC, corrCa_FC*corrCa_IB, corrCa_FC^2];

corr_Post_matrix_Ct = Post_matrix_Ct .* correction_matrix_Ct;
corr_Post_matrix_Ca = Post_matrix_Ca .* correction_matrix_Ca;


%normalized corrected posterior matrix
Norm_corr_Post_matrix_Ct = zeros(3);
Norm_corr_Post_matrix_Ca = zeros(3);

for i = 1:4
    for j = 1:4
        Norm_corr_Post_matrix_Ct(i,j) = corr_Post_matrix_Ct(i,j)/sum(corr_Post_matrix_Ct(:,j));
        Norm_corr_Post_matrix_Ca(i,j) = corr_Post_matrix_Ca(i,j)/sum(corr_Post_matrix_Ca(:,j));
    end
end


%normalized corrected posterior values for measurementdata-model comparison
Norm_corr_Post_MD_Ct = zeros(1,3);
Norm_corr_Post_MD_Ca = zeros(1,3);

for i = 1:3
    Norm_corr_Post_MD_Ct(i) = corr_Post_matrix_Ct(i+1,1)/sum(corr_Post_matrix_Ct(2:4,1));
    Norm_corr_Post_MD_Ca(i) = corr_Post_matrix_Ca(i+1,1)/sum(corr_Post_matrix_Ca(2:4,1));
end


%save
%save LastHalfOfPoints_all;

csvwrite('LastHalfOfPoints_corr_Post_matrix_Ct.csv',corr_Post_matrix_Ct);
csvwrite('LastHalfOfPoints_corr_Post_matrix_Ca.csv',corr_Post_matrix_Ca);

csvwrite('LastHalfOfPoints_Norm_corr_Post_matrix_Ct.csv',Norm_corr_Post_matrix_Ct);
csvwrite('LastHalfOfPoints_Norm_corr_Post_matrix_Ca.csv',Norm_corr_Post_matrix_Ca);

csvwrite('LastHalfOfPoints_Norm_corr_Post_MD_Ct.csv',Norm_corr_Post_MD_Ct);
csvwrite('LastHalfOfPoints_Norm_corr_Post_MD_Ca.csv',Norm_corr_Post_MD_Ca);

% save LastHalfOfPoints_SC_SurrogateCt.mat SC_SurrogateCt;
% save LastHalfOfPoints_FC_SurrogateCt.mat FC_SurrogateCt;
% save LastHalfOfPoints_IB_SurrogateCt.mat IB_SurrogateCt;
% 
% save LastHalfOfPoints_SC_SurrogateCa.mat SC_SurrogateCa;
% save LastHalfOfPoints_FC_SurrogateCa.mat FC_SurrogateCa;
% save LastHalfOfPoints_IB_SurrogateCa.mat IB_SurrogateCa;


