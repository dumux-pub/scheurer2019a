% ========================================================================
% ====== Goodness-of-fit of the models to measurement data
% ====== R2
% ========================================================================

clear all;


%load data
load SC_OutputCt.mat;
SC_OutputCt = OutputCt;
load SC_OutputCa.mat;
SC_OutputCa = OutputCa;
load FC_OutputCt.mat;
FC_OutputCt = OutputCt;
load FC_OutputCa.mat;
FC_OutputCa= OutputCa;
load IB_OutputCt.mat;
IB_OutputCt = OutputCt;
load IB_OutputCa.mat;
IB_OutputCa= OutputCa;

load observedCt.mat observedCt;
load measuredCa.mat measuredCa;

numberOfPointsTime = 35;
numberOfPointsSpaceKnown = 5;
numberOfPointsSpace = 57;

%only measured points in space, SC (model)
SC_ModelCt = [SC_OutputCt(:,4),SC_OutputCt(:,10),SC_OutputCt(:,16),SC_OutputCt(:,22),SC_OutputCt(:,28),SC_OutputCt(:,34),...
    SC_OutputCt(:,40),SC_OutputCt(:,46)];
SC_ModelCaa = [SC_OutputCa(:,9,:),SC_OutputCa(:,16,:),SC_OutputCa(:,24,:),SC_OutputCa(:,31,:),SC_OutputCa(:,39,:)];
%only measured points in space, FC (model)
FC_ModelCt = [FC_OutputCt(:,4),FC_OutputCt(:,10),FC_OutputCt(:,16),FC_OutputCt(:,22),FC_OutputCt(:,28),FC_OutputCt(:,34),...
    FC_OutputCt(:,40),FC_OutputCt(:,46)];
FC_ModelCaa = [FC_OutputCa(:,9,:),FC_OutputCa(:,16,:),FC_OutputCa(:,24,:),FC_OutputCa(:,31,:),FC_OutputCa(:,39,:)];
%only measured points in space, IB (model)
IB_ModelCt = [IB_OutputCt(:,4),IB_OutputCt(:,10),IB_OutputCt(:,16),IB_OutputCt(:,22),IB_OutputCt(:,28),IB_OutputCt(:,34),...
    IB_OutputCt(:,40),IB_OutputCt(:,46)];
IB_ModelCaa = [IB_OutputCa(:,9,:),IB_OutputCa(:,16,:),IB_OutputCa(:,24,:),IB_OutputCa(:,31,:),IB_OutputCa(:,39,:)];

%reshape Calcium, all points in time lined up
for i = 1:25            
    SC_ModelCa(i,:) = reshape(SC_ModelCaa(:,:,i)',1,5*numberOfPointsTime);
end
for i = 1:25
    FC_ModelCa(i,:) = reshape(FC_ModelCaa(:,:,i)',1,5*numberOfPointsTime);
end
for i = 1:16
    IB_ModelCa(i,:) = reshape(IB_ModelCaa(:,:,i)',1,5*numberOfPointsTime);
end

%initialize
observedCa = reshape(measuredCa',1,numel(measuredCa));
observedData = [observedCt,observedCa];
sCt = size(SC_ModelCt);
sCa = size(SC_ModelCa);
IB_sCt = size(IB_ModelCt);
IB_sCa = size(IB_ModelCa);

%calcite
Ct_length = 1;
for i = 1:sCt(1)
    SC_Ct_all(i) = sum((SC_ModelCt(i,:)-mean(observedCt)).^2);
    FC_Ct_all(i) = sum((FC_ModelCt(i,:)-mean(observedCt)).^2);
end
for i = 1:IB_sCt(1)
   IB_Ct_all(i) = sum((IB_ModelCt(i,:)-mean(observedCt)).^2); 
end

SC_Ct = mean(sum((observedCt-mean(observedCt)).^2)./SC_Ct_all);
IB_Ct = mean(sum((observedCt-mean(observedCt)).^2)./IB_Ct_all);
FC_Ct = mean(sum((observedCt-mean(observedCt)).^2)./FC_Ct_all);

%calcium
Ca_length = 1;
for i = 1:sCa(1)
    SC_Ca_all(i) = sum((SC_ModelCa(i,:)-mean(observedCa)).^2);
    FC_Ca_all(i) = sum((FC_ModelCa(i,:)-mean(observedCa)).^2);    
end
for i = 1:IB_sCa(1)
   IB_Ca_all(i) = sum((IB_ModelCa(i,:)-mean(observedCa)).^2); 
end

SC_Ca = mean(sum((observedCa-mean(observedCa)).^2)./SC_Ca_all);
IB_Ca = mean(sum((observedCa-mean(observedCa)).^2)./IB_Ca_all);
FC_Ca = mean(sum((observedCa-mean(observedCa)).^2)./FC_Ca_all);


%plot
%calcite
figure(1)
subplot(1,2,1);
b = bar([SC_Ct;IB_Ct;FC_Ct]);
b.FaceColor = 'flat';
b.CData(1,:) = [48/255,118/255,182/255];
b.CData(2,:) = [237/255,109/255,87/255];
b.CData(3,:) = [241/255,151/255,55/255];

title('Calcite content');
ylabel('\textbf{mean R$^2$}','interpreter','latex','fontweight','bold');
set(gca,'FontSize',20);
set(gca, 'FontName', 'Times New Roman');
set(gca,'XTickLabel',{'SC';'IB';'FC'})
grid();

%calcium
subplot(1,2,2);
b = bar([SC_Ca;IB_Ca;FC_Ca]);
b.FaceColor = 'flat';
b.CData(1,:) = [48/255,118/255,182/255];
b.CData(2,:) = [237/255,109/255,87/255];
b.CData(3,:) = [241/255,151/255,55/255];

title('Calcium concentration');
ylabel('\textbf{mean R$^2$}','interpreter','latex','fontweight','bold');
set(gca,'FontSize',20);
set(gca, 'FontName', 'Times New Roman');
set(gca,'XTickLabel',{'SC';'IB';'FC'})
grid();

sgt = sgtitle('mean R^2 between model and measurement data');
sgt.FontSize = 24;
set(sgt,'FontWeight','bold');
set(sgt, 'FontName', 'Times New Roman');
