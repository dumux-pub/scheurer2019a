% ========================================================================
% ====== Average model weight for the measurement data over increasing
% ====== amount of spacial data points
% ========================================================================

clear all;


%load data
load('AllPoints_all.mat','Norm_corr_Post_MD_Ct');
load('AllPoints_all.mat','Norm_corr_Post_MD_Ca');
AllPoints_Norm_corr_Post_MD_Ct = Norm_corr_Post_MD_Ct;
AllPoints_Norm_corr_Post_MD_Ca = Norm_corr_Post_MD_Ca;
load('LastHalfOfPoints_all.mat','Norm_corr_Post_MD_Ct');
load('LastHalfOfPoints_all.mat','Norm_corr_Post_MD_Ca');
LastHalfOfPoints_Norm_corr_Post_MD_Ct = Norm_corr_Post_MD_Ct;
LastHalfOfPoints_Norm_corr_Post_MD_Ca = Norm_corr_Post_MD_Ca;
load('LastPoint_all.mat','Norm_corr_Post_MD_Ct');
load('LastPoint_all.mat','Norm_corr_Post_MD_Ca');
LastPoint_Norm_corr_Post_MD_Ct = Norm_corr_Post_MD_Ct;
LastPoint_Norm_corr_Post_MD_Ca = Norm_corr_Post_MD_Ca;


%initialize
N = 3; %models
M = 3; %data set sizes
xax_Ct = 1:M+1;
xax_Ca = 1:M+1;

%diagonal elements
for i=1:N
    AllPoints_Ct(i) = AllPoints_Norm_corr_Post_MD_Ct(i);
    AllPoints_Ca(i) = AllPoints_Norm_corr_Post_MD_Ca(i);
    LastHalfOfPoints_Ct(i) = LastHalfOfPoints_Norm_corr_Post_MD_Ct(i);
    LastHalfOfPoints_Ca(i) = LastHalfOfPoints_Norm_corr_Post_MD_Ca(i);
    LastPoint_Ct(i) = LastPoint_Norm_corr_Post_MD_Ct(i);
    LastPoint_Ca(i) = LastPoint_Norm_corr_Post_MD_Ca(i);
end
Ct = [LastPoint_Ct,LastHalfOfPoints_Ct,AllPoints_Ct];
Ca = [LastPoint_Ca,LastHalfOfPoints_Ca,AllPoints_Ca];

SC_Ct = 1/3;
IB_Ct = 1/3;
FC_Ct = 1/3;
SC_Ca = 1/3;
IB_Ca = 1/3;
FC_Ca = 1/3;
for i=1:M
    SC_Ct(i+1) = Ct(i*N-2);
    IB_Ct(i+1) = Ct(i*N-1);
    FC_Ct(i+1) = Ct(i*N);
    SC_Ca(i+1) = Ca(i*N-2);
    IB_Ca(i+1) = Ca(i*N-1);
    FC_Ca(i+1) = Ca(i*N);
end

%calcite
figure(1)
subplot(1,2,1);
b = bar(xax_Ct,[SC_Ct;IB_Ct;FC_Ct]','stacked');
b(1).FaceColor = [48/255,118/255,182/255];
b(2).FaceColor = [237/255,109/255,87/255];
b(3).FaceColor = [241/255,151/255,55/255];

title('Calcite content');
legend('SC','IB','FC');
ylim([0, 1])
xlabel('number of data points N_{D,spacial}');
ylabel('model weight');
set(gca,'FontSize',20);
set(gca, 'FontName', 'Times New Roman');
set(gca,'XTickLabel',{'0';'1';'4';'8'})
grid();


%calcium
subplot(1,2,2);
b = bar(xax_Ca,[SC_Ca;IB_Ca;FC_Ca]','stacked');
b(1).FaceColor = [48/255,118/255,182/255];
b(2).FaceColor = [237/255,109/255,87/255];
b(3).FaceColor = [241/255,151/255,55/255];

title('Calcium concentration');
legend('SC','IB','FC');
ylim([0, 1])
xlabel('number of data points N_{D,spacial}');
ylabel('model weight');
set(gca,'FontSize',20);
set(gca, 'FontName', 'Times New Roman');
set(gca,'XTickLabel',{'0';'1';'3';'5'})
grid();

sgt = sgtitle('Evolution of model weights for measurement data');
sgt.FontSize = 24;
set(sgt,'FontWeight','bold');
set(sgt, 'FontName', 'Times New Roman');

