function M = declare_matrix(likelihood,N_RS,normal)

M = zeros(4);

M(1,1) = likelihood(1,1)/normal(1);
M(2,2) = mean(mean(likelihood(2:1+N_RS,2:1+N_RS)./normal(2:1+N_RS)));
M(3,3) = mean(mean(likelihood(2+N_RS:1+2*N_RS,2+N_RS:1+2*N_RS))./normal(2+N_RS:1+2*N_RS));
M(4,4) = mean(mean(likelihood(2+2*N_RS:1+3*N_RS,2+2*N_RS:1+3*N_RS))./normal(2+2*N_RS:1+3*N_RS));
M(1,2) = mean(likelihood(1,2:1+N_RS)./normal(2:1+N_RS));
M(2,1) = mean(likelihood(2:1+N_RS,1)./normal(1));
M(1,3) = mean(likelihood(1,2+N_RS:1+2*N_RS)./normal(2+N_RS:1+2*N_RS));
M(3,1) = mean(likelihood(2+N_RS:1+2*N_RS,1)./normal(1));
M(1,4) = mean(likelihood(1,2+2*N_RS:1+3*N_RS)./normal(2+2*N_RS:1+3*N_RS));
M(4,1) = mean(likelihood(2+2*N_RS:1+3*N_RS,1)./normal(1));
M(2,3) = mean(mean(likelihood(2:1+N_RS,2+N_RS:1+2*N_RS))./normal(2+N_RS:1+2*N_RS));
M(3,2) = mean(mean(likelihood(2+N_RS:1+2*N_RS,2:1+N_RS)./normal(2:1+N_RS)));
M(2,4) = mean(mean(likelihood(2:1+N_RS,2+2*N_RS:1+3*N_RS))./normal(2+2*N_RS:1+3*N_RS));
M(4,2) = mean(mean(likelihood(2+2*N_RS:1+3*N_RS,2:1+N_RS))./normal(2:1+N_RS));
M(3,4) = mean(mean(likelihood(2+N_RS:1+2*N_RS,2+2*N_RS:1+3*N_RS))./normal(2+2*N_RS:1+3*N_RS));
M(4,3) = mean(mean(likelihood(2+2*N_RS:1+3*N_RS,2+N_RS:1+2*N_RS))./normal(2+N_RS:1+2*N_RS));

end