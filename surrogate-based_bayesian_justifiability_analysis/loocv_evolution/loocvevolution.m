% ====================================================
%======loocv error evolution after every updating step
% ====================================================


%computation of mean loocv errors for different 
%models and both qoi
%-----------------------------------------------


%initialization
clear all;
SC_P = 15;
FC_P = 15;
IB_P = 6;
numberOfPointsTime = 35;
numberOfPointsSpace = 57;


%load data, have to be copied and renamed from the corresponding model
%folders
load SC_loocvCt.mat;
SC_loocvCt = loocvCt;
load SC_loocvCa.mat;
SC_loocvCa = loocvCa;
load FC_loocvCt.mat;
FC_loocvCt = loocvCt;
load FC_loocvCa.mat;
FC_loocvCa = loocvCa;
load IB_loocvCt.mat;
IB_loocvCt = loocvCt;
load IB_loocvCa.mat;
IB_loocvCa = loocvCa;

load SC_OutputCt.mat;
SC_OutputCt = OutputCt;
load SC_OutputCa.mat;
SC_OutputCa = OutputCa;
load FC_OutputCt.mat;
FC_OutputCt = OutputCt;
load FC_OutputCa.mat;
FC_OutputCa = OutputCa;
load IB_OutputCt.mat;
IB_OutputCt = OutputCt;
load IB_OutputCa.mat;
IB_OutputCa = OutputCa;


%loocv mean
SC_loocvCt_mean = mean(SC_loocvCt,2)';
SC_loocvCa_mean = mean(mean(SC_loocvCa,3),2)';
FC_loocvCt_mean = mean(FC_loocvCt,2)';
FC_loocvCa_mean = mean(mean(FC_loocvCa,3),2)';
IB_loocvCt_mean = mean(IB_loocvCt,2)';
IB_loocvCa_mean = mean(mean(IB_loocvCa,3),2)';


%loocv table
loocv(1,:) = SC_loocvCt_mean;
loocv(2,:) = IB_loocvCt_mean;
loocv(3,:) = FC_loocvCt_mean;
loocv(4,:) = SC_loocvCa_mean;
loocv(5,:) = IB_loocvCa_mean;
loocv(6,:) = FC_loocvCa_mean;


%save
csvwrite('loocvMean.csv',loocv);



%computation and plot of normalized mean loocv 
%errors for different models and both qoi
%-----------------------------------------------


%relative loocv
rel_loocv(1,:) = loocv(1,:)./mean(mean(SC_OutputCt));
rel_loocv(2,:) = loocv(2,:)./mean(mean(IB_OutputCt));
rel_loocv(3,:) = loocv(3,:)./mean(mean(FC_OutputCt));
rel_loocv(4,:) = loocv(4,:)./mean(mean(mean(SC_OutputCa)));
rel_loocv(5,:) = loocv(5,:)./mean(mean(mean(IB_OutputCa)));
rel_loocv(6,:) = loocv(6,:)./mean(mean(mean(FC_OutputCa)));


%save
csvwrite('relLoocvMean.csv',rel_loocv);


%plot of normalized mean loocv errors
%------------------------------------


%number of updating steps
xax = 1:10;

%calcite
subplot(1,2,1);
semilogy(xax,rel_loocv(1,2:11),'--gs','MarkerSize',15,'LineWidth',2,'Color',[48/255,118/255,182/255]);
hold on;
semilogy(xax,rel_loocv(2,2:11),'--gs','MarkerSize',15,'LineWidth',2,'Color',[237/255,109/255,87/255]);
semilogy(xax,rel_loocv(3,2:11),'--gs','MarkerSize',15,'LineWidth',2,'Color',[241/255,151/255,55/255]);
hold off;
title('Calcite content');
legend('SC','IB','FC');
xlabel('number of updating steps');
ylabel('relative mean LOOCV error');
set(gca,'FontSize',20);
set(gca, 'FontName', 'Times New Roman');
grid();

%calcium
subplot(1,2,2);
semilogy(xax,rel_loocv(4,2:11),'--gs','MarkerSize',15,'LineWidth',2,'Color',[48/255,118/255,182/255]);
hold on;
semilogy(xax,rel_loocv(5,2:11),'--gs','MarkerSize',15,'LineWidth',2,'Color',[237/255,109/255,87/255]);
semilogy(xax,rel_loocv(6,2:11),'--gs','MarkerSize',15,'LineWidth',2,'Color',[241/255,151/255,55/255]);
hold off;
title('Calcium concentration');
legend('SC','IB','FC');
xlabel('number of updating steps');
ylabel('relative mean LOOCV error');
set(gca,'FontSize',20);
set(gca, 'FontName', 'Times New Roman');
grid();

sgt = sgtitle('Relative mean LOOCV error ');
sgt.FontSize = 24;
set(sgt,'FontWeight','bold');
set(sgt, 'FontName', 'Times New Roman');









