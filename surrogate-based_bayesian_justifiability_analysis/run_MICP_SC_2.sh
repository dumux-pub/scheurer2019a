#!/bin/bash

#SBATCH --account=simprod
#SBATCH --partition=TS
#SBATCH --time=2-0
#SBATCH --ntasks=1
#SBATCH --nodes=1

# Umleiten der Ausgabedateien
#                                #TODO change that
#SBATCH --output=/nfs/home_simtech/scheurer/logfiles/output/slurm-o%j.out
#SBATCH --error=/nfs/home_simtech/scheurer/logfiles/error/slurm-e%j.out

#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=stefaniascheurer@gmail.com

NODENAME=`hostname`
STARTTIME=`date`

echo " Script started at $STARTTIME"
echo " on host $NODENAME"
echo ""

WORKDIR=/lustre/ws1/scheurer/simplified_chemistry_model
# DUNEDIR=/lustre/ws1/scheurer/scheurer2019a


# ulimit ist nur bei Jobs mit einer Laufzeit ueber 10 Tage notwendig
ulimit -t unlimited

# Load modules
MATLAB="/sw/simtech/apps/matlab/R2019a/bin/matlab -nodisplay -nosplash"
# module load gcc/7.2/0
#module load matlab/2016/b

cd $WORKDIR

# Call Matalab. ----------------------------------------------------------------------------
# $MATLAB -r "main_Teng; exit"
echo 'Start Matlab Simulation'
# matlab -r "main_Teng; exit"
$MATLAB -r "SCM_MainRun_2; exit"
echo 'End Matlab Simulation'
# End Call Matalab. ----------------------------------------------------------------------------

cd ~

ENDTIME=`date`
echo ""
echo " Script ended at $ENDTIME"
