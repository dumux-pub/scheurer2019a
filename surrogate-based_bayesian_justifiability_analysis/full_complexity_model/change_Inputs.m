function [] = change_Inputs(Inputfile, searched_param, new_value)
 
fid = fopen(Inputfile);
data = textscan(fid, '%s', 'Delimiter', '\n', 'CollectOutput', true);
fclose(fid);
 
for i = 1:length(data{1})
    found_param = strcmp(data{1}{i}, searched_param);

    if found_param == 1
        data{1}{i} = new_value;
    end
end
 
fid = fopen(Inputfile, 'w');
for i = 1:length(data{1})
    fprintf(fid, '%s\n', char(data{1}{i}));
end
fclose(fid);

end