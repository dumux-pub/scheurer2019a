[Newton]
MaxRelativeShift = 1e-8 #
MaxSteps = 5 #

[Model]
PlausibilityTolerance = 1e-6 #
pHMin = 2 #
pHMax = 12 #
uCurExtrapolation = 1 # 1: set uCur_ to a linear extrapolation from the last two timesteps 0: use the standard uCur_ = uPrev_

[Problem]
Name = Column8 #
Inverse = 0 # 0 for normal model, 1 for use as forward model in an inverse model run

[Initial]
initDensityW = 1087 # 			[kg/m]
initPressure = 1e5 # 			[Pa]

initxwTC = 2.3864e-7 #	 		[mol/mol]
initxwNa = 0.0 # 				[mol/mol]
initxwCl = 0.0 # 				[mol/mol]
initxwCa = 0.0 # 				[mol/mol]
initxwUrea = 0.0 # 				[mol/mol]
initxwTNH = 3.341641e-3 #	 	[mol/mol]
initxwO2 = 4.4686e-6 # 			[mol/mol]
initxwSubstrate = 2.97638e-4 # 	[mol/mol]
initxwSuspendedBiomass = 0.0 # 			[mol/mol]

initBiofilm = 0.0 # 			[-]
initCalcite = 0.0 # 			[-]
initTemperature = 298.15 #      [K] 25C

xwNaCorr = 2.9466e-6 # 			[mol/mol]      //NaCorr to get the pH to 6.2 calculated as molefraction
xwClCorr = 0.0 # 				[mol/mol]

[Injection]
injVolumeflux = 1.716666666667e-7 # =10.3/60/1e6 #	//[m/s] = [ml/min] /[s/min] /[ml/m]

injTC = 5.8e-7 #				 [kg/kg]		//equilibrium with atmospheric CO2 unter atmospheric pressure
injNa = 0.0 #				 [kg/m]		//NaCl injected
injCa = 13.530742		 #		 [kg/m]		//computed from 49 g/l CaCl2*2H2O (molar mass = 147.68g/mol --> 0.33molCa/l, equimolar with urea (20g/l and 60g/mol))
injUrea = 20 #					 [kg/m]
injTNH = 3.183840574 #//3.184#	 [kg/m]		//computed from 10 g/l NH4Cl
injO2 = 0.008 #					 [kg/m]
injSubstrate = 3 #					 [kg/m]
injSuspendedBiomass = 0.0665 #				 [kg/m]#Column8		//5.6e7 cfu/ml (40e8cfu/ml~1g/l)

injNaCorr= 0.00379 #			[kg/m]		//NaCorr to get the pH to 6.2
injTemperature= 298.15 #        [K]
injPressure= 1e5 #              [Pa]

numInjections = 212 #
InjectionParamFile = ../injections/Column8Inj.dat #

[TimeLoop]
DtInitial = 1 #0.01# [s]
TEnd = 3203460 # [s]		# Column8
MaxTimeStepSize = 200 # [s]

[Grid]
UpperRight = 0.7112  # [m] upper right corner coordinates
Cells = 56  # [-] number of cells in x,y-direction

[SpatialParams]
Porosity = 0.4 # [-]
CritPorosity = 0.0 # [-]

Permeability = 2e-10 # [m^2]

[BioCoefficients]
ca1 = 8.3753e-8 #0.04434 # 		// [1/s] 		kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!
ca2 = 8.5114e-7 #9.187e-4 # 	// [1/s] 		kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!
cd1 = 2.894e-8 # 				// [1/s] 		Ebigbo et al. 2010
dc0 = 3.183e-7 # 				// [1/s] 		Taylor and Jaffe 1990
kmue = 4.1667e-5  #5.787e-5 #	// [1/s] 		Connolly et al. 2013
F = 0.5 #						// [-] 			Mateles 1971
Ke = 2e-5 #						// [kg/m] 		Hao et al. 1983
KpHa = 6.15e-10 # 				//[mol/kgH2O] Kim et al. 2000
Ks = 7.99e-4 #					// [kg/m] 		Taylor and Jaffe 1990
Yield = 0.5 #					// [-] 			Seto and Alexander 1985
rhoBiofilm = 6.9 #10 # 			//[kg/m3] 		kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!

[CalciteCoefficients]
ac = 2000 # 		// [1/dm] 		Ebigbo et al. 2012  (estimated)
kdiss1 = 8.9e-3 # 	// [kgH2O/dms] Chou et al. 1989
kdiss2 = 6.5e-9 #  	// [mol/dms] 	Chou et al. 1989
kprec = 1.5e-12 # 	// [mol/dms] 	Zhong and Mucci 1989
ndiss = 1.0 # 		// [-] 			Flukinger and Bernard 2009
nprec = 3.27 # 		// [-] 			Zhong and Mucci 1989
Asw0 = 500.0 # 		// [1/dm] 		Ebigbo et al. 2012  (estimated using phi_0 and A/V)

[UreolysisCoefficients]
kub = 3.81e-4 # 			// [kg_urease/kg_bio] 		calculated from kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!
kurease = 706.6667 #41.67 #	// [mol_urea/(kg_urease s)] Lauchnor et al. 2014
nub = 1.0 # 				// [-] 						Lauchnor et al. 2014
Keu1 = 1e-88 #7.57e-7 # 	// [mol/kgH2O] 				removed from equation  Lauchnor et al. 2014
Keu2 = 1e-88 #1.27e-8 # 	// [mol/kgH2O] 				removed from equation  Lauchnor et al. 2014
KNH4 = 10000 #0.0122 # 		// [mol/kgH2O] 				no effect observed in exp, set to high value to remove any effect on r_urea Lauchnor et al. 2014
Ku = 0.355 #0.0173 #		// [mol/kgH2O] 				Lauchnor et al. 2014

