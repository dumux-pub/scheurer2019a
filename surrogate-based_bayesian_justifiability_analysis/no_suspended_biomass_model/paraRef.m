% ========================================================================
% Arbitraty Integrative Probabilistic Collocation Method
% Author: Dr. Sergey Oladyshkin
% SRC SimTech,Universitaet Stuttgart, Pfaffenwaldring 61, 70569 Stuttgart
% E-mail: Sergey.Oladyshkin@iws.uni-stuttgart.de
% ========================================================================
%modified by Lena Walter
clear all;

%-------------------------------------------------------------------------------------------
%---------- Reading VTU's
%-------------------------------------------------------------------------------------------


%         %--->Reading of Names for Simulation Output Files
% % Column 4 results (in Diss column C4)
% fid = fopen('Column4.pvd', 'r');

% % Column 8 (in Diss column D1) results
fid = fopen('Column8_NoSuspBiomass.pvd', 'r');

% Column 9 (in Diss column D2) results
%fid = fopen('Column9.pvd', 'r');
i=0;
while feof(fid) == 0
    line = fgetl(fid);
    if (findstr(line,'DataSet')~=0)
        i=i+1;
        ref_Time(i)=str2num(line(findstr(line,'timestep="')+length('timestep="'):findstr(line,'" g')-1));
        ref_SimulationOutputFileNames{i}=line(findstr(line,'file="')+length('file="'):findstr(line,'"/>')-1);
    end
end
fclose(fid);

%--->Reading Number of Points/Cells and Box/Cell-Volumes
fid = fopen(strcat(ref_SimulationOutputFileNames{1}), 'rt');
while feof(fid) == 0
    line = fgetl(fid);
    if (findstr(line,'NumberOfPoints=')~=0)
        ref_NumberOfPoints=str2num(line(findstr(line,'NumberOfPoints="')+length('NumberOfPoints="'):findstr(line,'">')-1));
    end
    if (findstr(line,'Name="boxVolume"')~=0)
        ref_vol=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
    end
    if (findstr(line,'NumberOfCells=')~=0)
        ref_NumberOfCells=str2num(line(findstr(line,'NumberOfCells="')+length('NumberOfCells="'):findstr(line,'">')-1));
    end
    if (findstr(line,'Name="cell')~=0) & (~isempty(strfind(line,'volume')~=0))
        ref_cellVolume=fscanf(fid, '%g', [1 ref_NumberOfCells]);
    end
    
end
fclose(fid);

%---> Reading from all Output Simulation Files
for j=1:1:length(ref_SimulationOutputFileNames)
    fid = fopen(strcat(ref_SimulationOutputFileNames{j}), 'rt');
    i=0;
    while feof(fid) == 0
        i=i+1;
        line = fgetl(fid);
        
        
        %         if (findstr(line,'Name="p_w"')~=0)
        %             ref_p_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %         if (findstr(line,'Name="p_n"')~=0)
        %             ref_p_n(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="S_w"')~=0)
        %             ref_S_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %         if (findstr(line,'Name="S_n"')~=0)
        %             ref_S_n(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="rho_w"')~=0)
        %             ref_rho_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %         if (findstr(line,'Name="rho_n"')~=0)
        %             ref_rho_n(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="rhoMolar_w"')~=0)
        %             ref_rhoMol_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %         if (findstr(line,'Name="rhoMolar_n"')~=0)
        %             ref_rhoMol_n(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^Cl-_w"')~=0)
        %             ref_xCl_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^Na+_w"')~=0)
        %             ref_xNa_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^TotalC_w"')~=0)
        %             ref_xTotalC_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^Substrate_w"')~=0)
        %             ref_xSubstrate_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^O2_w"')~=0)
        %             ref_xO2_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^SuspendedBiomass_w"')~=0)
        %             ref_xSuspendedBiomass_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="molarity^Urea_w"')~=0)
        %             ref_molarity^Urea_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        
        if (findstr(line,'Name="molarity_w^Ca2+"')~=0)
            ref_molarityCa2_w(j,:) = fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        end
        
        %         if (findstr(line,'Name="molarity^NH4+_w"')~=0)
        %             ref_molarityNH4_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="molarity^TotalNH_w"')~=0)
        %             ref_molarityTotalNH_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^Urea_w"')~=0)
        %             ref_xUrea_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^Ca2+_w"')~=0)
        %             ref_xCa2_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^NH4+_w"')~=0)
        %             ref_xNH4_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^TotalNH_w"')~=0)
        %             ref_xTotalNH_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^CO2_w"')~=0)
        %             ref_xCO2_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^HCO3-_w"')~=0)
        %             ref_xHCO3_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^CO3-_w"')~=0)
        %             ref_xCO3_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^H+_w"')~=0)
        %             ref_xH_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %         if (findstr(line,'Name="x^OH-_w"')~=0)
        %             ref_xOH_w(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %
        %         if (findstr(line,'Name="Permeability"')~=0)
        %             ref_Permeability(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %         if (findstr(line,'Name="Kxx"')~=0)
        %             ref_Permeability(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %         if (findstr(line,'Name="porosity"')~=0)
        %             ref_porosity(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        %
        %
        %         if (findstr(line,'Name="Coordinates"')~=0)
        %             ref_Coordinates(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints*3]);
        %         end
        %
        %
        %         if (findstr(line,'Name="precipitateVolumeFraction^Biofilm"')~=0)
        %             ref_volFracBio(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        %         end
        
        if (findstr(line,'Name="precipitateVolumeFraction^Calcite"')~=0)
            ref_volFracCalcite(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        end
        
    end
    fclose(fid);
    
end

save('MICPResult.mat');


