function [loo_error] = loocv(Psi, Output)

M = length(Psi);
s = size(Output);

%initialize
loo_error = zeros(1,s(2));
error = zeros(1,M);

for space = 1:s(2)
    
    for test = 1:M
        clear TestPoint;
        TestPoint = Psi(test,:);
        TestOutput = Output(test,space);
        if test == 1
            PsiTest = Psi(2:M,:);
            OutputTest = Output(2:M,space);
        elseif test == M
            PsiTest = Psi(1:M-1,:);
            OutputTest = Output(1:M-1,space);
        else
            PsiTest = [Psi(1:test-1,:);Psi(test+1:M,:)];
            OutputTest = [Output(1:test-1,space);Output(test+1:M,space)];
        end
        
        CoeffsTest = linsolve(PsiTest,OutputTest); %coefficients calculated without TestPoint
        OutputCompared = TestPoint*CoeffsTest; %Output with coefficients at TestPoint
        error(test) = (TestOutput - OutputCompared)^2;
    end
    
    loo_error(space) = mean(error);
   
end


end




