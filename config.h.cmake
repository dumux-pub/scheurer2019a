/* begin scheurer2019a
   put the definitions for config.h specific to
   your project here. Everything above will be
   overwritten
*/

/* begin private */
/* Name of package */
#define PACKAGE "@DUNE_MOD_NAME@"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "@DUNE_MAINTAINER@"

/* Define to the full name of this package. */
#define PACKAGE_NAME "@DUNE_MOD_NAME@"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "@DUNE_MOD_NAME@ @DUNE_MOD_VERSION@"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "@DUNE_MOD_NAME@"

/* Define to the home page for this package. */
#define PACKAGE_URL "@DUNE_MOD_URL@"

/* Define to the version of this package. */
#define PACKAGE_VERSION "@DUNE_MOD_VERSION@"

/* end private */

/* Define to the version of scheurer2019a */
#define DUMUX_SCHEURER2019_VERSION "@DUMUX_SCHEURER2019_VERSION@"

/* Define to the major version of scheurer2019a */
#define DUMUX_SCHEURER2019_VERSION_MAJOR @DUMUX_SCHEURER2019_VERSION_MAJOR@

/* Define to the minor version of scheurer2019a */
#define DUMUX_SCHEURER2019_VERSION_MINOR @DUMUX_SCHEURER2019_VERSION_MINOR@

/* Define to the revision of scheurer2019a */
#define DUMUX_SCHEURER2019A @DUMUX_SCHEURER2019A@

/* end scheurer2019a
   Everything below here will be overwritten
*/
